package ru.softbalance.localization;

import android.content.Context;

import java.util.Locale;
import java.util.MissingResourceException;

public class Localization {

    private static Localization instance;

    private Context context;

    private Localization(Context context) {
        this.context = context;
    };

    public static Localization getInstance(Context context) {
        if (instance == null) {
            instance = new Localization(context);
        }

        return instance;
    }

    public String getNativeLanguageName(Locale locale) {
        return locale.getDisplayLanguage(locale);
    }

    public int getFlagDrawableResId(Locale locale) {
        try {
            String drawableName = locale.getISO3Language();
            return context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName());
        } catch (MissingResourceException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getLanguageNameISO2(String iso3LanguageName) {
        String iso2LanguageName = iso3LanguageName.substring(0,2);

        String[] countryCodes = context.getResources().getStringArray(R.array.country_codes);
        for (String countryCodeComplex : countryCodes) {
            String[] splittedCounryCodes = countryCodeComplex.split("\\,");
            if (splittedCounryCodes[1].equalsIgnoreCase(iso3LanguageName)) {
                iso2LanguageName = splittedCounryCodes[0];
                break;
            }
        }

        return iso2LanguageName;
    }
}
